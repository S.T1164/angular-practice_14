import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { from, Observable, Observer } from 'rxjs';

class MyData{
  data : string;
  list : Person[] = [];
}

class Person{
  name : string;
  mail : string;
  tel : string;
}

@Injectable({
  providedIn: 'root'
})
export class MycheckService {

  private myData : MyData = new MyData();

  constructor(private client : HttpClient) {
    let ob : Observable<any> = from(fetch('/assets/data.json'));
    ob.subscribe((resp) => {
      resp.json().then((val) => {
        this.myData = val;
      })
    })
   }

  get(n : number){
    return this.myData.list[n];
  }

  get size(){
    return this.list.length;
  }

  get data(){
    return this.myData.data;
  }

  get list(){
    return this.myData.list;
  }
  
}
