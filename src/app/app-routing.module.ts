import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HelloComponent } from './hello/hello.component';
import { MessageComponent } from './message/message.component';


const routes: Routes = [
  { path : 'hello' , component: HelloComponent},
  { path : 'msg' , component: MessageComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes,{
    enableTracing : true
  })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
